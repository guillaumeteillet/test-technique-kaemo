<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends BehatContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
     * @Given /^I have a path file "([^"]*)"$/
     */
    public function iHaveAPathFile($arg1)
    {
        $arg1 = "/Users/guillaume/Desktop/test/test.mp4";
    }

    /**
     * @Given /^I have a title "([^"]*)"$/
     */
    public function iHaveATitle($arg1)
    {
        $arg1 = "La Course aux étoiles";
    }

    /**
     * @Given /^I have a realisator "([^"]*)"$/
     */
    public function iHaveARealisator($arg1)
    {
        $arg1 = "Henri Durand";
    }

    /**
     * @Given /^The date is (\d+)\/(\d+)\/(\d+) (\d+):(\d+)$/
     */
    public function theDateIs($arg1, $arg2, $arg3, $arg4, $arg5)
    {
        $arg1 = date('d/m/Y H:m', time());
    }

    /**
     * @When /^I run \'([^\']*)\'$/
     */
    public function iRun($arg1)
    {
        //throw new PendingException();
    }

    /**
     * @Then /^I should get:$/
     */
    public function iShouldGet(PyStringNode $string)
    {
        //throw new PendingException();
    }

}
