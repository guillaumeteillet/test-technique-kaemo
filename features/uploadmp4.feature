# features/uploadmp4.feature
Feature: upload:mp4
  You can upload mp4 files in an Amazon S3 Bucket.
Scenario: List 2 files in a directory
  Given I have a path file "/Users/guillaume/Desktop/test/test.mp4"
  And I have a title "La Course aux étoiles"
  And I have a realisator "Henri Durand"
  And The date is 02/08/15 15:30
  When I run 'app/console upload:mp4 "/Users/guillaume/Desktop/test/test.mp4" "La Course aux étoiles" "Henri Durand"' 
  Then I should get:
    """
    Path of the file : /Users/guillaume/Desktop/test/test.mp4
    The title : La Course aux étoiles
    Realisator : Henri Durand
    Date and time : 02-08-2015 15:30
    You can download the file here : https://s3-eu-west-1.amazonaws.com/kaemotesttechnique/test.mp4
    """