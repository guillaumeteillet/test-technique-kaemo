<?php

namespace Kaemo\APIBundle\Command;

use DateTime;
use Kaemo\APIBundle\Entity\Videos;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;

class UploadCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('upload:mp4')
            ->setDescription('Upload MP4 on AMAZON S3 BUCKET')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Where is the file ?'
            )
            ->addArgument(
                'title',
                InputArgument::REQUIRED,
                'What is the title ?'
            )
            ->addArgument(
                'realisator',
                InputArgument::REQUIRED,
                'What is the name of the realisator ?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        if ($path) {
            $path_console = 'Path of the file : '.$path;
        }

        $title = $input->getArgument('title');
        if ($path) {
            $title_console = 'The title : '.$title;
        }

        $realisator = $input->getArgument('realisator');
        if ($path) {
            $realisator_console = 'Realisator : '.$realisator;
        }

        $date_console = "Date and time : ".date('d-m-Y H:m', time());

        $video = new Videos();
        $video->setTitle($title);
        $video->setDate(new DateTime());
        $video->setRealisator($realisator);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($video);
        $em->flush();


        $storage = $this->getContainer()->get('kaemo_storage');
        $test = $storage->uploadFile('/Users/guillaume/Desktop/test/test.mp4');

        $output->writeln($path_console);
        $output->writeln($title_console);
        $output->writeln($realisator_console);
        $output->writeln($date_console);
        $output->writeln("You can download the file here : ".$test);
    }
}

?>