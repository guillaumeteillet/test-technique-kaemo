<?php

namespace Kaemo\APIBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testAPIVideo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/videos');
        $response = $client->getResponse();
        $data = json_decode( $response->getContent() );
        $this->assertTrue( count( $data ) > 0);
    }

    public function testAPIVideoId()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/videos/1');
        $response = $client->getResponse();
        $data = json_decode( $response->getContent() );
        $this->assertTrue( count( $data ) > 0);
    }
}
