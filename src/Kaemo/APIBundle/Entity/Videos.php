<?php

namespace Kaemo\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;


/**
 * Videos
 *
 * @ORM\Table()
*
 * @ORM\Entity(repositoryClass="Kaemo\APIBundle\Entity\VideosRepository")
 *
 * @ExclusionPolicy("all")
 */
class Videos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Expose
     */
    private $title;

    /**
     * @var date
     *
     * @ORM\Column(name="date", type="date", length=255)
     *
     * @Expose
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="realisator", type="string", length=255)
     *
     * @Expose
     */
    private $realisator;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Videos
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param string date
     * @return Videos
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set Realisator
     *
     * @param string $realisator
     * @return Videos
     */
    public function setRealisator($realisator)
    {
        $this->realisator = $realisator;

        return $this;
    }

    /**
     * Get realisator
     *
     * @return string 
     */
    public function getRealisator()
    {
        return $this->realisator;
    }


}
