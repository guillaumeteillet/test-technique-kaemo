<?php

namespace Kaemo\APIBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class VideosRepository extends EntityRepository
{

    public function findBetweenDate($from, $to)
    {
        $qb = $this->createQueryBuilder('a');

        $this->whereCurrentYear($qb, $from, $to);

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function whereCurrentYear(QueryBuilder $qb, $from, $to)
    {
        $date_start = new \DateTime(substr($from, 0, 4).'-'.substr($from, 4, 2).'-'.substr($from, 6, 2));
        $date_end = new \DateTime(substr($to, 0, 4).'-'.substr($to, 4, 2).'-'.substr($to, 6, 2));

        $qb
            ->where('a.date BETWEEN :start AND :end')
            ->setParameter('start', $date_start)
            ->setParameter('end',   $date_end);
    }
}