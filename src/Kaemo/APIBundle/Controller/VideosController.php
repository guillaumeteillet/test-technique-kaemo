<?php

namespace Kaemo\APIBundle\Controller;

use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class VideosController extends Controller  implements ClassResourceInterface
{
    public function cgetAction()
    {
        if (isset($_GET['from']) && isset($_GET['to']))
        {
            $from = $_GET['from'];
            $to = $_GET['to'];
            $repository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('KaemoAPIBundle:Videos')
            ;

            $video = $repository->findBetweenDate($from, $to);
        }
        else if (isset($_GET['realisator']))
        {
            $realisator = $_GET['realisator'];
            $video = $this->getDoctrine()->getRepository('KaemoAPIBundle:Videos')->findBy(array('realisator' => $realisator));
        }
        else
            $video = $this->getDoctrine()->getRepository('KaemoAPIBundle:Videos')->findAll();

        $result = array('videos' => $video, 'count' => sizeof($video));

        return $result;
    }

    public function getAction($id)
    {

        $video = $this->getDoctrine()->getRepository('KaemoAPIBundle:Videos')->find($id);
        if(!is_object($video)){
            throw $this->createNotFoundException();
        }

        $result = array('video' => $video);

        return $result;
    }
}
